0.1.0.4

* Bump versions

0.1.0.3

* Change the type of `alignWith` to be more consistent.

0.1.0.2

* Add `allThoseAOr` and `allThoseBOr`.

0.1.0.1

* Add `alignWith`.
* Remove extraneous dependency.

0.1.0.0

* This change log starts.
* The initial version of alignment.
